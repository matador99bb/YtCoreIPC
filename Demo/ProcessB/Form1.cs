﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yt.Core.IPC;
using TestDll;
namespace ProcessB
{
    public partial class Form1 : Form
    {
        IpcProxy ipcProxy;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string pipename = "yourpipename";

              ipcProxy = IpcProxy.CreateClient(pipename);
            ipcProxy.OnConnection =()=>
            {
                log("跨进程通信已经连接");
            };
            ipcProxy.OnDisconnect = () => 
            {
                log("跨进程通信已经断开");
            };
            ipcProxy.OnError = (ex) => 
            {
                log("跨进程通信遇到错误:"+ex.Message);
            };
            Logger logger = new Logger();
            logger.Log = (info) => 
            {
                log("收到回信："+info);
            };
            ipcProxy.AddService<ILog>(logger);
            ipcProxy.Connect();

        }
        private void log(string info)
        {
            richTextBox1.AppendText(info + "\n");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var result = ipcProxy.RemoteInvoke<long, TestDll.ITest>(x => x.Add(12, 15));
            log("远程计算：12+15=" + result);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ipcProxy.RemoteInvoke< TestDll.ITest>(x => x.SendString(textBox1.Text));
            log("你向对方发送了信息："+ textBox1.Text);

        }
    }
}
