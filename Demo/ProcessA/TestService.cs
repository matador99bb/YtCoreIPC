﻿using System;

using TestDll;

using Yt.Core.IPC;

namespace ProcessA
{
    public class TestService : TestDll.ITest
    {
        public IpcProxy ipcProxy { get; set; }
       
        public Action<string> OnReceived { get; set; }
        public long Add(long x, long y)
        {
            OnReceived($"收到Add消息{x}+{y}");
            return x * y;
        }

        public void SendString(string msg)
        {
            OnReceived("收到了ProcessB的信息：" + msg);
            ipcProxy.RemoteInvoke<ILog>(x => x.LogInfo("ProcessA：收到了ProcessB的信息。"));
        }
    }
}
